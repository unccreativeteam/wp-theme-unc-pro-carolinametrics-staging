			<footer class="footer" role="contentinfo">
<div id="unc-logo"> <a href="http://www.unc.edu"><img src="<?php echo get_template_directory_uri(); ?>/library/images/unc.png" alt="The University of North Carolina at Chapel Hill"></a> </div><!--unc logo -->
				<div id="inner-footer" class="clearfix">
 
<div id="infowrap">
					<nav role="navigation">
							<?php bones_footer_links(); ?>
					</nav>
					<p class="source-org copyright">University Operator: (919)962-2211 &copy; <?php echo date('Y'); ?> The University of North Carolina at Chapel Hill.</p>

					

			</footer>
</div><!--end of infowrap -->
		

		<?php // all js scripts are loaded in library/bones.php ?>
		<?php wp_footer(); ?>

	</body>

</html>
