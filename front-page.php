<?php
/*
Template Name: login front page
*/
?>

<?php get_header(); ?>

<div id="mainwrapper" class="radial-center">
<div id="wrapper">

<div class="circle1 transparent_class"><div class="circletextwrapper"><h1 class="circletext"><a href="<?php echo esc_url( home_url( '/' ) ); ?>student-quality-outcomes/">Student Quality<br />&amp; Outcomes</a></h1></div></div>
   <div class="circle2 transparent_class"><div class="circletextwrapper"><h1 class="circletext"><a href="<?php echo esc_url( home_url( '/' ) ); ?>public-benefits/">Public<br />Benefits</a></h1></div></div>
     <div class="circleMiddle transparent_class2"><div class="circletextwrapper"><h1 class="circletext"><a href="<?php echo esc_url( home_url( '/' ) ); ?>top-10">Top 10</a></h1></div></div>
   <div class="circle3 transparent_class"><div class="circletextwrapper"><h1 class="circletext circletextcampus"><a href="<?php echo esc_url( home_url( '/' ) ); ?>campus-environment/">Campus <br />Environment</a></h1></div></div>
      <div class="circle4 transparent_class"><div class="circletextwrapper"><h1 class="circletext circletextstudent"><a href="<?php echo esc_url( home_url( '/' ) ); ?>faculty-quality-outcomes/">Faculty Quality<br />&amp; Outcomes</a></h1></div></div>
      
</div><!--close of wrapper-->

 <div id="tagline"><?php bloginfo('description'); ?></div>	<!--close of tag-->	
						<?php // to use a image just replace the bloginfo('name') with your img src and remove the surrounding <p> ?>
					<p id="logo" class="h1"><a href="<?php echo home_url("student-quality-outcomes"); ?>" rel="nofollow"><?php bloginfo('name'); ?></a></p>
				<div id="intro">	
				<!--jquery pop up- li item -->	
         <li> 
		 <div class="p_anch">
			 <a href="#" class="js__p_intro_start">Introduction</a>
		 </div>
		 
		 <div class="p_body js__p_body js__fadeout"></div>
		 
		  <div class="popup js__intro_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content"><p>The Carolina Metrics were developed to enable us to review our success in fulfilling Carolina’s mission and strategic priorities. The indicators were identified following a campus-wide effort to solicit input from faculty, senior administrators, staff, and students in response to the question “How can we measure Carolina’s academic performance?”  </p>
<p>Using multiple measures in four key domains to evaluate our own progress, as well as to benchmark against our peers, gives us an evidence-based foundation to support decision-making, to allocate resources, and to set targets for improvement.  The Carolina Metrics also provide another means of demonstrating to our stakeholders our commitment to accountability, transparency, and value. </p>  
<p>The Carolina Metrics data will be updated in March and September each year. Consistent with our ongoing efforts to assess and enhance our effectiveness, we will also evaluate the individual measures in terms of the usefulness of the information they provide.    Therefore, as a tool for continuous improvement, the Carolina Metrics will always be a “work in progress.”</p>
</div>
			 </div>
		</li>	
				</div>	
		<!--jquery pop up ends--> 


 

 </div><!--close of mainwrapper-->

			

<?php include("footer2.php");?>

