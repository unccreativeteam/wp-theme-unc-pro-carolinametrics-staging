/*
Bones Scripts File
Author: Eddie Machado

This file should contain any js scripts you want to add to the site.
Instead of calling it in the header or throwing it inside wp_head()
this file will be called automatically in the footer so as not to
slow the page load.

*/

// IE8 ployfill for GetComputed Style (for Responsive Script below)
if (!window.getComputedStyle) {
	window.getComputedStyle = function(el, pseudo) {
		this.el = el;
		this.getPropertyValue = function(prop) {
			var re = /(\-([a-z]){1})/g;
			if (prop == 'float') prop = 'styleFloat';
			if (re.test(prop)) {
				prop = prop.replace(re, function () {
					return arguments[2].toUpperCase();
				});
			}
			return el.currentStyle[prop] ? el.currentStyle[prop] : null;
		}
		return this;
	}
}


// as the page loads, call these scripts
jQuery(document).ready(function($) {

/* Mean Menu */
    jQuery('header nav').meanmenu();

// fittext js
jQuery("#logo").fitText(2.2);

	/*
	Responsive jQuery is a tricky thing.
	There's a bunch of different ways to handle
	it, so be sure to research and find the one
	that works for you best.
	*/

	/* getting viewport width */
	var responsive_viewport = $(window).width();

	/* if is below 481px */
	if (responsive_viewport < 481) {

	} /* end smallest screen */

	/* if is larger than 481px */
	if (responsive_viewport > 481) {

	} /* end larger than 481px */

	/* if is above or equal to 768px */
	if (responsive_viewport >= 768) {

		/* load gravatars */
		$('.comment img[data-gravatar]').each(function(){
			$(this).attr('src',$(this).attr('data-gravatar'));
		});

	}

	/* off the bat large screen actions */
	if (responsive_viewport > 1030) {

	}


	// add all your scripts here


jQuery('a').on('click touchend', function(e) {
      var el = $(this);
      var link = el.attr('href');
      window.location = link;
   });


// popup box
jQuery(".js__p_fo1_start, .js__p_fo2_start, .js__p_fo3_start, .js__p_fo4_start, .js__p_fo5_start, .js__p_fo6_start, .js__p_fo7_start, .js__p_fo8_start, .js__p_fo9_start, .js__p_fo10_start, .js__p_fo11_start, .js__p_fo12_start, .js__p_fo13_start, .js__p_fo14_start, .js__p_fo15_start, .js__p_fo16_start, .js__p_fo17_start, .js__p_fo18_start, .js__p_fo19_start, .js__p_fo20_start, .js__p_fo21_start, .js__p_fo22_start, .js__p_ce1_start, .js__p_ce2_start, .js__p_ce3_start, .js__p_ce4_start, .js__p_ce5_start, .js__p_ce6_start, .js__p_ce7_start, .js__p_ce8_start, .js__p_ce9_start, .js__p_ce10_start, .js__p_ce11_start, .js__p_ce12_start, .js__p_ce13_start, .js__p_ce14_start, .js__p_ce15_start, .js__p_ce16_start, .js__p_ce17_start, .js__p_ce18_start, .js__p_ce19_start,.js__p_ce20_start, .js__p_sq1_start, .js__p_sq2_start, .js__p_sq3_start, .js__p_sq4_start, .js__p_sq5_start, .js__p_sq6_start, .js__p_sq7_start, .js__p_sq8_start, .js__p_sq9_start, .js__p_sq10_start, .js__p_sq11_start, .js__p_sq12_start, .js__p_sq13_start, .js__p_sq14_start, .js__p_sq15_start, .js__p_sq16_start, .js__p_sq17_start, .js__p_sq18_start, .js__p_sq19_start, .js__p_sq20_start, .js__p_sq21_start, .js__p_sq22_start, .js__p_sq23_start, .js__p_sq24_start, .js__p_pb1_start, .js__p_pb2_start, .js__p_pb3_start, .js__p_pb4_start, .js__p_pb5_start, .js__p_pb6_start, .js__p_pb7_start, .js__p_pb8_start, .js__p_pb9_start, .js__p_pb10_start, .js__p_pb11_start, .js__p_pb12a_start, .js__p_pb12b_start, .js__p_pb13_start, .js__p_pb14_start, .js__p_pb15_start, .js__p_pb16_start, .js__p_pb17_start, .js__p_pb18_start, .js__p_pb19_start, .js__p_pb20_start, .js__p_pb21_start, .js__p_top1_start, .js__p_top2_start, .js__p_top3_start, .js__p_top4_start, .js__p_top5_start, .js__p_top6_start, .js__p_top7_start, .js__p_top8_start, .js__p_top9_start, .js__p_top10_start, .js__p_intro_start ").simplePopup();

// removing side sub menu on click
$('ul ul li a').on('click', function () {
    $(this).closest("ul").addClass("hidden");
});
$('ul li').on('mouseover', function () {
    $(this).find('ul').removeClass("hidden");
});

// ipad double tab issue solves
jQuery( '#flyout_menu li:has(ul)' ).doubleTapToGo();




}); /* end of as page load scripts */


/*! A fix for the iOS orientationchange zoom bug.
 Script by @scottjehl, rebound by @wilto.
 MIT License.
*/
(function(w){
	// This fix addresses an iOS bug, so return early if the UA claims it's something else.
	if( !( /iPhone|iPad|iPod/.test( navigator.platform ) && navigator.userAgent.indexOf( "AppleWebKit" ) > -1 ) ){ return; }
	var doc = w.document;
	if( !doc.querySelector ){ return; }
	var meta = doc.querySelector( "meta[name=viewport]" ),
		initialContent = meta && meta.getAttribute( "content" ),
		disabledZoom = initialContent + ",maximum-scale=1",
		enabledZoom = initialContent + ",maximum-scale=10",
		enabled = true,
		x, y, z, aig;
	if( !meta ){ return; }
	function restoreZoom(){
		meta.setAttribute( "content", enabledZoom );
		enabled = true; }
	function disableZoom(){
		meta.setAttribute( "content", disabledZoom );
		enabled = false; }
	function checkTilt( e ){
		aig = e.accelerationIncludingGravity;
		x = Math.abs( aig.x );
		y = Math.abs( aig.y );
		z = Math.abs( aig.z );
		// If portrait orientation and in one of the danger zones
		if( !w.orientation && ( x > 7 || ( ( z > 6 && y < 8 || z < 8 && y > 6 ) && x > 5 ) ) ){
			if( enabled ){ disableZoom(); } }
		else if( !enabled ){ restoreZoom(); } }
	w.addEventListener( "orientationchange", restoreZoom, false );
	w.addEventListener( "devicemotion", checkTilt, false );
})( this );
