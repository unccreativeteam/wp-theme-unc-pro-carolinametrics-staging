	<div id="navcol1" role="navigation"><div id="logointernal"><a href="<?php echo get_option('home'); ?>/">Carolina Metrics</a></div>
	
	<div id='flyout_menu'>
<ul>
  <li><a href='<?php echo esc_url( home_url( '/' ) ); ?>top-10/'><span>Top Ten</span></a></li>
   <li><a href='<?php echo esc_url( home_url( '/' ) ); ?>student-quality-outcomes/'><span>Student Quality<br />&amp; Outcomes</span></a>
      <ul>
               <li><a href='<?php echo esc_url( home_url( '/' ) ); ?>student-quality-outcomes/#s4'><span>All Metrics</span></a></li>

         <li><a href='<?php echo esc_url( home_url( '/' ) ); ?>student-quality-outcomes/#s1'><span>Qualifications</span></a></li>

<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>student-quality-outcomes/#s4'><span>Diversity and Access</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>student-quality-outcomes/#s9'><span>Engagement</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>student-quality-outcomes/#s10'><span>Global</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>student-quality-outcomes/#s11'><span>Learning Outcomes</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>student-quality-outcomes/#s12'><span>Satisfaction</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>student-quality-outcomes/#s16'><span>Honors and Awards</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>student-quality-outcomes/#s17'><span>Completion Rates</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>student-quality-outcomes/#s22'><span>Alumni Success</span></a></li>
   </ul></li>
   <li><a href='<?php echo esc_url( home_url( '/' ) ); ?>campus-environment/'><span>Campus <br />Environment</span></a>
      <ul>
         <li><a href='<?php echo esc_url( home_url( '/' ) ); ?>campus-environment/#c1'><span>Intellectual Climate and Energy</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>campus-environment/#c4'><span>Inviting Environment</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>campus-environment/#c8'><span>Attractiveness to Applicants</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>campus-environment/#c14'><span>Welcoming Differences</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>campus-environment/#c16'><span>Interdisciplinary Engagement</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>campus-environment/#c18'><span>Leadership Quality</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>campus-environment/#c20'><span>Campus Safety</span></a></li>
   </ul></li>
    <li><a href='<?php echo esc_url( home_url( '/' ) ); ?>faculty-quality-outcomes/'><span>Faculty Quality<br />&amp; Outcomes</span></a>
   <ul>
   <li><a href='<?php echo esc_url( home_url( '/' ) ); ?>faculty-quality-outcomes/#f1'><span>Selectivity</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>faculty-quality-outcomes/#f3'><span>Salary Competitiveness</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>faculty-quality-outcomes/#f4'><span>Retention</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>faculty-quality-outcomes/#f5'><span>Demographic Diversity</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>faculty-quality-outcomes/#f7'><span>Appointment Status</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>faculty-quality-outcomes/#f9'><span>Faculty Satisfaction</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>faculty-quality-outcomes/#f12'><span>Faculty-Student Interactions</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>faculty-quality-outcomes/#f14'><span>Student Ratings of Faculty Interactions</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>faculty-quality-outcomes/#f17'><span>Research Productivity</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>faculty-quality-outcomes/#f21'><span>Honors And Awards</span></a></li>
   </ul></li>
   <li><a href='<?php echo esc_url( home_url( '/' ) ); ?>public-benefits/'><span>Public<br />Benefits</span></a>
      <ul>
         <li><a href='<?php echo esc_url( home_url( '/' ) ); ?>public-benefits/#p1'><span>Research Revenue</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>public-benefits/#p5'><span>State Support</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>public-benefits/#p6'><span>Affordability</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>public-benefits/#p10'><span>Contributions from <br />Students & Graduates</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>public-benefits/#p14'><span>Institutional Reputation</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>public-benefits/#p18'><span>University Resources that Benefit the Public</span></a></li>
   </ul></li>
</ul>
</div>	
		</div><!--close of navcol1-->