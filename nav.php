
	<div id="navcol1" role="navigation"><div id="logointernal"><a href="<?php echo get_option('home'); ?>/">Carolina Metrics</a></div>
	
		
	
	
	<div id='flyout_menu'>
<ul>
<li <?php if ($thisPage=="top10") echo " id=\"currentpage\""; ?>>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>top-10/"><span>Top 10</span></a>

  <ul>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>top-10'><span>All Metrics</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>top-10/#top1'><span>Completion Rates</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>top-10/#top2'><span>Diversity and Access</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>top-10/#top3'><span>Demographic Diversity</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>top-10/#top3'><span>University Resources That Benefit The Public</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>top-10/#top6'><span>Research Revenue</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>top-10/#top9'><span>Institutional Reputation</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>top-10/#top10'><span>University Resources That Benefit The Public</span></a></li>
   </ul></li>
  
  
  <li <?php if ($thisPage=="student") echo " id=\"currentpage\""; ?>>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>student-quality-outcomes/"><span>Student Quality<br />&amp; Outcomes</span></a>
       <ul>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>student-quality-outcomes/#s'><span>All Metrics</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>student-quality-outcomes/#s1'><span>Qualifications</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>student-quality-outcomes/#s4'><span>Diversity and Access</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>student-quality-outcomes/#s9'><span>Global</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>student-quality-outcomes/#s10'><span>Learning Outcomes</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>student-quality-outcomes/#s11'><span>Satisfaction</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>student-quality-outcomes/#s13'><span>Honors and Awards</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>student-quality-outcomes/#s14'><span>Completion Rates</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>student-quality-outcomes/#s19'><span>Alumni Success</span></a></li>
   </ul></li>
   <li <?php if ($thisPage=="campus") echo " id=\"currentpage\""; ?>>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>campus-environment/"><span>Campus <br />Environment</span></a>
      <ul>
               <li><a href='<?php echo esc_url( home_url( '/' ) ); ?>campus-environment/#c'><span>All Metrics</span></a></li>

         <li><a href='<?php echo esc_url( home_url( '/' ) ); ?>campus-environment/#c1'><span>Intellectual Climate and Energy</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>campus-environment/#c4'><span>Inviting Environment</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>campus-environment/#c8'><span>Attractiveness to Applicants</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>campus-environment/#c10'><span>Welcoming Differences</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>campus-environment/#c11'><span>Interdisciplinary Engagement</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>campus-environment/#c13'><span>Leadership Quality</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>campus-environment/#c14'><span>Campus Safety</span></a></li>
   </ul></li>
    <li  <?php if ($thisPage=="faculty") echo " id=\"currentpage\""; ?>>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>faculty-quality-outcomes/"><span>Faculty Quality<br />&amp; Outcomes</span></a>
   <ul>
   <li><a href='<?php echo esc_url( home_url( '/' ) ); ?>faculty-quality-outcomes/#f'><span>All Metrics</span></a></li>
   
   <li><a href='<?php echo esc_url( home_url( '/' ) ); ?>faculty-quality-outcomes/#f1'><span>Selectivity</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>faculty-quality-outcomes/#f2'><span>Salary Competitiveness</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>faculty-quality-outcomes/#f3'><span>Retention</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>faculty-quality-outcomes/#f4'><span>Demographic Diversity</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>faculty-quality-outcomes/#f6'><span>Appointment Status</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>faculty-quality-outcomes/#f8'><span>Faculty Satisfaction</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>faculty-quality-outcomes/#f10'><span>Faculty-Student Interactions</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>faculty-quality-outcomes/#f12'><span>Student Ratings of Faculty Interactions</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>faculty-quality-outcomes/#f14'><span>Research Revenue</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>faculty-quality-outcomes/#f15'><span>Research Productivity</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>faculty-quality-outcomes/#f17'><span>Honors And Awards</span></a></li>
   </ul></li>
 <li <?php if ($thisPage=="public") echo " id=\"currentpage\""; ?>>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>public-benefits/"><span>Public<br />Benefits</span></a>
      <ul>
               <li><a href='<?php echo esc_url( home_url( '/' ) ); ?>public-benefits/p'><span>All Metrics</span></a></li>
 <li><a href='<?php echo esc_url( home_url( '/' ) ); ?>public-benefits/#p1'><span>Research Revenue</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>public-benefits/#p5'><span>State Support</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>public-benefits/#p6'><span>Affordability</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>public-benefits/#p10'><span>Contributions from <br />Students & Graduates</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>public-benefits/#p14'><span>Institutional Reputation</span></a></li>
<li><a href='<?php echo esc_url( home_url( '/' ) ); ?>public-benefits/#p18'><span>University Resources that Benefit the Public</span></a></li>
   </ul></li>
</ul>
</div>	

    <button class="button"><?php wp_loginout(); ?></button>
		</div><!--close of navcol1-->