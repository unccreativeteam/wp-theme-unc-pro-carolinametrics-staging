<?php
/*
Template Name: Campus Environment
*/
?>

<?php get_header(); ?>


	<?php include("nav.php");?>
	
			<div id="banner"></div>

	<div id="contentwrap" class="clearfix">
    <h1 class="headline" id="logo">Campus Enviroment</h1>
	<div class="statement">To nurture student learning and enhance our faculty’s ability to excel in their teaching and research responsibilities, Carolina must ensure that the intellectual community in which students and faculty live and work is vibrant, stimulating, collaborative, and sufficiently resourced.  A welcoming campus environment provides the best foundation for teaching, learning, cutting-edge research, and service.</div>
	
 
	<!------------------------ ce1 chart half width----------------------- -->	
	
		<div class="chartWrapperHalf">
		<a name="c1">
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/CampusEnvironment-01.svg" /><div class="chartFooter"><ul>
  	  <!--jquery pop up- li item -->	
         <li> 
		 <div class="p_anch">
			 <a href="#" class="js__p_ce1_start">Source</a>
		 </div>
		 
		 <div class="p_body js__p_body js__fadeout"></div>
		 
		  <div class="popup js__ce1_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">Collaborative on Academic Careers in Higher Education (COACHE) Survey, administered during 2012-13.</div>
			 </div>
		</li>		
		<!--jquery pop up ends--> 
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/CampusEnvironment_1.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://localhost:8888/campus-environment/#c1." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
		
	<!------------------------ce2 chart half width----------------------- -->	

		<div class="chartWrapperHalf">
		<a name="c2">
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/CampusEnvironment-02.svg" /><div class="chartFooter"><ul>
  	  <!--jquery pop up- li item -->	
             <li> 
		 <div class="p_anch">
			 <a href="#" class="js__p_ce2_start">Source</a>
		 </div>
		 
		 <div class="p_body js__p_body js__fadeout"></div>
		 
		  <div class="popup js__ce2_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">Collaborative on Academic Careers in Higher Education (COACHE) Survey, administered during 2012-13.</div>
			 </div>
		</li>	
		<!--jquery pop up ends--> 
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/CampusEnvironment_2.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://localhost:8888/campus-environment/#c2." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
		
		<!------------------------ce3 chart full width----------------------- -->	
	<div class="chartWrapperFull">
	<a name="c3">
    <img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/CampusEnvironment-03.svg" />
   
    <div class="chartFooter"><ul>
        <li> 
		 <div class="p_anch">
			 <a href="#" class="js__p_ce3_start">Source</a>
		 </div>
		 
		 <div class="p_body js__p_body js__fadeout"></div>
		 
		  <div class="popup js__ce3_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">The Student Experience in the Research University Survey (SERU), administered spring 2013.</div>
			 </div>
		</li>	
       
        <li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/CampusEnvironment_3.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://localhost:8888/campus-environment/#c3." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
	<!------------------------ ce4 chart half width----------------------- -->	
	
		<div class="chartWrapperHalf">
		<a name="c4">
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/CampusEnvironment-04.svg" /><div class="chartFooter"><ul>
  	  <!--jquery pop up- li item -->	
         <li> 
		 <div class="p_anch">
			 <a href="#" class="js__p_ce4_start">Source</a>
		 </div>
		 
		 <div class="p_body js__p_body js__fadeout"></div>
		 
		  <div class="popup js__ce4_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content"><em>U.S. News & World Report</em>, America's Best Colleges 2014. <a href="http://www.thebestcolleges.org/most-beautiful-campuses/"> thebestcolleges.org/most-beautiful-campuses/</a></div>
			 </div>
		</li>		
		<!--jquery pop up ends--> 
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/CampusEnvironment_4.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://localhost:8888/campus-environment/#c4." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
		
	<!------------------------ce5 chart half width----------------------- -->	

		<div class="chartWrapperHalf">
		<a name="c5">
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/CampusEnvironment-05.svg" /><div class="chartFooter"><ul>
  	  <!--jquery pop up- li item -->	
             <li> 
		 <div class="p_anch">
			 <a href="#" class="js__p_ce5_start">Source</a>
		 </div>
		 
		 <div class="p_body js__p_body js__fadeout"></div>
		 
		  <div class="popup js__ce5_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content"><em>Forbes</em>, America’s Most Beautiful College Campuses.<br /><a href="http://www.forbes.com/pictures/ekkf45i/southern-florida-college-lakeland-fla/"> forbes.com/pictures/ekkf45i/southern-florida-college-lakeland-fla/<a></div>
			 </div>
		</li>	
		<!--jquery pop up ends--> 
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/CampusEnvironment_5.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://localhost:8888/campus-environment/#c5." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->

		
		<!------------------------ce6 chart full width----------------------- -->	
	<div class="chartWrapperFull">
	<a name="c6">
    <img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/CampusEnvironment-06.svg" />
   
    <div class="chartFooter"><ul>
        <li> 
		 <div class="p_anch">
			 <a href="#" class="js__p_ce6_start">Source</a>
		 </div>
		 
		 <div class="p_body js__p_body js__fadeout"></div>
		 
		  <div class="popup js__ce6_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">Universitas Indonesia Green Metric World University Ranking 2013. <a href="http://greenmetric.ui.ac.id/ranking/year/2013"> greenmetric.ui.ac.id/ranking/year/2013</div>
			 </div>
		</li>	
       
        <li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/CampusEnvironment_6.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://localhost:8888/campus-environment/#c6." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->	
		
		
		<!------------------------ce7 chart full width----------------------- -->	
	<div class="chartWrapperFull">
	<a name="c7">
    <img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/CampusEnvironment-07.svg" />
   
    <div class="chartFooter"><ul>
        <li> 
		 <div class="p_anch">
			 <a href="#" class="js__p_ce7_start">Source</a>
		 </div>
		 
		 <div class="p_body js__p_body js__fadeout"></div>
		 
		  <div class="popup js__ce7_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content"><em>Princeton Review</em>, Green Honor Roll 2014</div>
			 </div>
		</li>	
       
        <li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/CampusEnvironment_7.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://localhost:8888/campus-environment/#c7." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->		
		
			
			
	<!------------------------ ce8 chart half width----------------------- -->	
	
		<div class="chartWrapperHalf">
		<a name="c8">
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/CampusEnvironment-08.svg" /><div class="chartFooter"><ul>
  	  <!--jquery pop up- li item -->	
         <li> 
		 <div class="p_anch">
			 <a href="#" class="js__p_ce8_start">Source</a>
		 </div>
		 
		 <div class="p_body js__p_body js__fadeout"></div>
		 
		  <div class="popup js__ce8_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">Compiled by the UNC-Chapel Hill Office of Institutional Research and Assessment using the Common Data Set from individual institutions' websites and the US News and World Report College Compass.</div>
			 </div>
		</li>		
		<!--jquery pop up ends--> 
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/CampusEnvironment_8.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://localhost:8888/campus-environment/#c8." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
		
	<!------------------------ce9 chart half width----------------------- -->	

		<div class="chartWrapperHalf">
		<a name="c9">
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/CampusEnvironment-09.svg" /><div class="chartFooter"><ul>
  	  <!--jquery pop up- li item -->	
             <li> 
		 <div class="p_anch">
			 <a href="#" class="js__p_ce9_start">Source</a>
		 </div>
		 
		 <div class="p_body js__p_body js__fadeout"></div>
		 
		  <div class="popup js__ce9_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">Compiled by the UNC-Chapel Hill Office of Institutional Research and Assessment using the Common Data Set from individual institutions' websites and the US News and World Report College Compass.</div>
			 </div>
		</li>	
		<!--jquery pop up ends--> 
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/CampusEnvironment_9.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://localhost:8888/campus-environment/#c9." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
		
			<!------------------------ce10 chart full width----------------------- -->	
	<div class="chartWrapperFull">
	<a name="c10">
    <img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/CampusEnvironment-10.svg" />
   
    <div class="chartFooter"><ul>
        <li> 
		 <div class="p_anch">
			 <a href="#" class="js__p_ce10_start">Source</a>
		 </div>
		 
		 <div class="p_body js__p_body js__fadeout"></div>
		 
		  <div class="popup js__ce10_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">Compiled by the UNC-CH Office of Institutional Research and Assessment from ConnectCarolina student information system.</div>
			 </div>
		</li>	
       
        <li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/CampusEnvironment_10.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://localhost:8888/campus-environment/#c10." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->	
		
		<!------------------------ce11 chart full width----------------------- -->	
	<div class="chartWrapperFull">
	<a name="c11">
    <img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/CampusEnvironment-11.svg" />
   
    <div class="chartFooter"><ul>
        <li> 
		 <div class="p_anch">
			 <a href="#" class="js__p_ce11_start">Source</a>
		 </div>
		 
		 <div class="p_body js__p_body js__fadeout"></div>
		 
		  <div class="popup js__ce11_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">Compiled by the UNC-CH Office of Institutional Research and Assessment from ConnectCarolina student information system.</div>
			 </div>
		</li>	
       
        <li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/CampusEnvironment_11.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://localhost:8888/campus-environment/#c11." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->			
		
		
		
<!------------------------end of charts content----------------------- -->					
</div>	<!--close of content wrap-->
<?php get_footer(); ?>


