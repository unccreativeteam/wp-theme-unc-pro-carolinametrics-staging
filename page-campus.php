<?php
/*
Template Name: Campus Environment
*/
?>
<?php $thisPage="campus"; ?>
<?php get_header(); ?>
<div class="meantitle"><a href="http://carolinametrics.unc.edu" rel="nofollow"><?php bloginfo('name'); ?></a></div>


<a id="c" class="shifted_anchor"></a>

	<?php include("nav.php");?>
			<div class="banner bannerCampus"></div>

	<div id="contentwrap" class="clearfix">
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <h1 class="headline" id="logo"><?php the_title();?> </h1>
	<div class="statement"><?php the_content(); ?></div>
	<?php endwhile; else: ?>
	<p>Sorry, this page does not exist</p>

<?php endif; ?>

    <!----------------- ce1 chart half width----------------------- -->
		<div class="chartWrapperHalf">
		 <a id="c1" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/campusenvironment_1.png" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->
         <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_ce1_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__ce1_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">Collaborative on Academic Careers in Higher Education (COACHE) Survey, administered to UNC-Chapel Hill tenured and tenure track faculty during 2012-13.</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/campusenvironment_1.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/campus-environment/#c1." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->

	<!------------------------ce2 half width----------------------- -->
		<div class="chartWrapperHalf">
		 <a id="c2" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/campusenvironment_2.svg" /><div class="chartFooter"><ul>
		 <!--jquery pop up- li item -->
             <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_ce2_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__ce2_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">The Student Experience in the Research University Survey (SERU), administered to UNC-Chapel Hill undergraduates in Spring 2013.</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/campusenvironment_2.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/campus-environment/#c2." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->

			<!------------------------ ce3 chart half width----------------------- -->

		<div class="chartWrapperHalf">
		 <a id="c3" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/campusenvironment_3.png" /><div class="chartFooter"><ul>
  	  <!--jquery pop up- li item -->
         <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_ce3_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__ce3_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">SOURCE: America's Best Colleges 2016. <a href="http://www.thebestcolleges.org/most-beautiful-campuses/">thebestcolleges.org/most-beautiful-campuses/</a>, published in 2016</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/campusenvironment_3.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/campus-environment/#c3." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->

		<!------------------------ce4 chart half width----------------------- -->
	<div class="chartWrapperHalf">
	 <a id="c4" class="shifted_anchor"></a>
    <img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/campusenvironment_4.png" />

    <div class="chartFooter"><ul>
        <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_ce4_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__ce4_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">Association for the Advancement of Sustainability in Higher Education's (AASHE) STARS Rating. <a href="https://stars.aashe.org/institutions/participants-and-reports/"> stars.aashe.org/institutions/participants-and-reports/</a>, August 2016.</div>
			 </div>
		</li>

        <li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/campusenvironment_4.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/campus-environment/#c4." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->

	<!------------------------ce5 chart half width ----------------------- -->

		<div class="chartWrapperHalf">
		 <a id="c5" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/campusenvironment_5.png" /><div class="chartFooter"><ul>
  	  <!--jquery pop up- li item -->
             <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_ce5_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__ce5_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content"><em>US News &amp; World Report</em>, America's Best Colleges, 2016 edition. Based on Fall 2014 data.</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/campusenvironment_5.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/campus-environment/#c5." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->

		<!------------------------ ce6 chart half width----------------------- -->
		<div class="chartWrapperHalf">
	 <a id="c6" class="shifted_anchor"></a>
    <img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/campusenvironment_6.png" />

    <div class="chartFooter"><ul>
        <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_ce6_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__ce6_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content"><em>US News &amp; World Report</em>, America's Best Colleges, 2016 edition. Based on Fall 2014 data.</div>
			 </div>
		</li>

        <li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/campusenvironment_6.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/campus-environment/#c6." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->



		<!------------------------ ce7 chart half width----------------------- -->

		<div class="chartWrapperHalf">
		 <a id="c7" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/campusenvironment_7.svg" /><div class="chartFooter"><ul>
  	  <!--jquery pop up- li item -->
         <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_ce7_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__ce7_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">Compiled by the UNC-CH Office of Institutional Research and Assessment from ConnectCarolina student information system. Based on Fall 2014 data.</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/campusenvironment_7.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/campus-environment/#c7." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->

	<!------------------------ce8 chart half width----------------------- -->

		<div class="chartWrapperHalf">
		 <a id="c8" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/campusenvironment_8.svg" /><div class="chartFooter"><ul>
  	  <!--jquery pop up- li item -->
             <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_ce8_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__ce8_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">Compiled by the UNC-CH Office of Institutional Research and Assessment from ConnectCarolina student information system. Based on Fall 2014 data.</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/campusenvironment_8.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/campus-environment/#c8." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->

	<!------------------------ ce9 chart half width----------------------- -->

		<div class="chartWrapperHalf">
		 <a id="c9" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/campusenvironment_9.png"/><div class="chartFooter"><ul>
  	  <!--jquery pop up- li item -->
         <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_ce9_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__ce9_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">Collaborative on Academic Careers in Higher Education (COACHE) Survey, administered to UNC-Chapel Hill tenured and tenure track faculty during 2012-13.</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/campusenvironment_9.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/campus-environment/#c9." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->

	<!------------------------ce10 chart half width----------------------- -->

		<div class="chartWrapperHalf">
		 <a id="c10" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/campusenvironment_10.svg"/><div class="chartFooter"><ul>
  	  <!--jquery pop up- li item -->
             <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_ce10_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__ce10_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">The Student Experience in the Research University Survey (SERU), administered to UNC-Chapel Hill undergraduates in Spring 2013.</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/campusenvironment_10.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/campus-environment/#c10." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->

	<!------------------------ce11 chart full width----------------------- -->
	<div class="chartWrapperFull">
	 <a id="c11" class="shifted_anchor"></a>
    <img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/campusenvironment_11.png" />

    <div class="chartFooter"><ul>
        <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_ce11_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__ce11_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">Collaborative on Academic Careers in Higher Education (COACHE) Survey, administered to UNC-Chapel Hill tenured and tenure track faculty during 2012-13.</div>
			 </div>
		</li>

        <li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/campusenvironment_11.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/campus-environment/#c11." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->

<!------------------------ce12 chart full width----------------------- -->
	<div class="chartWrapperFull">
	 <a id="c12" class="shifted_anchor"></a>
    <img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/campusenvironment_12.png" />

    <div class="chartFooter"><ul>
        <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_ce12_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__ce12_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">UNC-Chapel Hill Office of Research Information Systems, Vice Chancellor for Research.</div>
			 </div>
		</li>

        <li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/campusenvironment_12.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/campus-environment/#c12." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
<!------------------------ce13 chart half width----------------------- -->
	<div class="chartWrapperHalf">
	 <a id="c13" class="shifted_anchor"></a>
    <img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/campusenvironment_13.svg" />

    <div class="chartFooter"><ul>
        <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_ce13_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__ce13_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">UNC-Chapel Hill Employee Survey conducted by Carolina Counts in 2013.</div>
			 </div>
		</li>

        <li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/campusenvironment_13.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/campus-environment/#c13." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->

		<!------------------------ ce14 chart half width----------------------- -->

		<div class="chartWrapperHalf">
		 <a id="c14" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/campusenvironment_14.svg"/><div class="chartFooter"><ul>
  	  <!--jquery pop up- li item -->
         <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_ce14_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__ce14_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">The Student Experience in the Research University Survey (SERU), administered to UNC-Chapel Hill undergraduates in Spring 2013.</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/campusenvironment_14.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/campus-environment/#c14." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->

	<!------------------------ce15 chart half width----------------------- -->

<!------------------------end of charts content----------------------- -->
</div>	<!--close of content wrap-->
<?php get_footer(); ?>
