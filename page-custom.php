<?php
/*
Template Name: Faculty Quality & Outcomes
*/
?>

<?php get_header(); ?>


	<?php include("nav.php");?>
			<div id="banner"></div>

	<div id="contentwrap" class="clearfix">

	<h1 class="statement">A statement about the <br />Faculty Quality & Outcomes metrics.</h1>
	<!--full chart-->
	<div class="chartWrapperFull">
    <img src="<?php echo get_template_directory_uri(); ?>/library/images/faculty-01.svg" />
   
    <div class="chartFooter"><ul>
         <!--jquery pop up- li item -->	
         <li>
		<div class="p_anch"> <a href="#" class="js__p_start">Source</a></div>
		<div class="p_body js__p_body js__fadeout"></div>
		<div class="popup js__popup js__slide_top"> <a href="#" class="p_close js__p_close" title="Close"></a>
		<div class="p_content">source info goes here</div>
		</div>
		</li>		
		<!--jquery pop up ends-->
        <li><a href="#"><span aria-hidden="true" data-icon="&#xe601"></span></a></li>
		<li><a href="mailto:?subject=I wanted you to see this site&amp;body=Check out this site http://localhost:8888/faculty-quality-outcomes/." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></		span></a></li></ul></div>
		</div><!--end of chartFooter-->

	<!-- 1st double charts -->	
	
		<div class="chartWrapperHalf">
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/faculty-02.svg" /><div class="chartFooter"><ul>
  	  <!--jquery pop up- li item -->	
         <li>
		<div class="p_anch"> <a href="#" class="js__p_start">Source</a></div>
		<div class="p_body js__p_body js__fadeout"></div>
		<div class="popup js__popup js__slide_top"> <a href="#" class="p_close js__p_close" title="Close"></a>
		<div class="p_content">source info goes here</div>
		</div>
		</li>		
		<!--jquery pop up ends--> 
		<li><a href="#"><span aria-hidden="true" data-icon="&#xe601"></span></a></li><li><span aria-hidden="true" data-icon="&#xe600"></span></li></ul></div></div>
	 	<!-- 2nd double charts -->

		<div class="chartWrapperHalf">
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/faculty-02.svg" /><div class="chartFooter"><ul>
  	  <!--jquery pop up- li item -->	
         <li>
		<div class="p_anch"> <a href="#" class="js__p_start">Source</a></div>
		<div class="p_body js__p_body js__fadeout"></div>
		<div class="popup js__popup js__slide_top"> <a href="#" class="p_close js__p_close" title="Close"></a>
		<div class="p_content">source info goes here</div>
		</div>
		</li>		
		<!--jquery pop up ends--> 
		<li><a href="#"><span aria-hidden="true" data-icon="&#xe601"></span></a></li><li><span aria-hidden="true" data-icon="&#xe600"></span></li></ul></div></div>
			</div>	<!--close of content wrap-->			
			

				

<?php get_footer(); ?>



<a href="mailto:?subject=I wanted you to see this site&amp;body=Check out this site http://localhost:8888/faculty-quality-outcomes/." title="Share by Email"><img src="library/images/faculty-01.png"/></a>