<?php
/*
Template Name: Faculty Quality & Outcomes
*/
?>
<?php $thisPage="faculty"; ?>
<?php get_header(); ?>
<div class="meantitle"><a href="http://carolinametrics.unc.edu" rel="nofollow"><?php bloginfo('name'); ?></a></div>


<a id="f" class="shifted_anchor"></a>
	<?php include("nav.php");?>
			<div class="banner bannerFaculty"></div>

	<div id="contentwrap" class="clearfix">
   <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <h1 class="headline" id="logo"><?php the_title();?> </h1>
	<div class="statement"><?php the_content(); ?></div>
	<?php endwhile; else: ?>
	<p>Sorry, this page does not exist</p>

<?php endif; ?>

	<!------------------------f1 chart full width----------------------- -->

	<div class="chartWrapperFull">
		<a id="f1" class="shifted_anchor"></a>

    <img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/facultyqualityandoutcomes_1.png"/>

    <div class="chartFooter"><ul>
      <!--jquery pop up- li item -->
         <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_fo1_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__fo1_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">Compiled by the UNC-Chapel Hill Office of the Executive Vice Chancellor and Provost for the 2014-15 academic year.</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
        <li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/facultyqualityandoutcomes_1.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/faculty-quality-outcomes/#f1." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->


<!------------------------ f2 chart fullwidth -------------------------- -->

		<div class="chartWrapperFull">
		<a id="f2" class="shifted_anchor"></a>

		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/facultyqualityandoutcomes_2.png" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->
         <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_fo2_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__fo2_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">"The Annual Report on the Economic Status of the Profession, 2014-15,” Academe, March-April, 2015. Based on Fall 2014 data. Note: Total faculty compensation is the sum of the annualized base salary plus the estimated value of benefits provided.</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/facultyqualityandoutcomes_2.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/faculty-quality-outcomes/#f2." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->


		<!------------------------fo3 chart full width----------------------- -->
	<div class="chartWrapperFull">
			<a id="f3" class="shifted_anchor"></a>


    <img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/facultyqualityandoutcomes_3.png" />

    <div class="chartFooter"><ul>
      <!--jquery pop up- li item -->
         <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_fo3_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__fo3_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">Compiled by the UNC-Chapel Hill Office of the Executive Vice Chancellor and Provost.</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
        <li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/facultyqualityandoutcomes_3.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/faculty-quality-outcomes/#f3." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->


<!------------------------ fo4 chart half width----------------------- -->

		<div class="chartWrapperHalf">
				<a id="f4" class="shifted_anchor"></a>

		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/facultyqualityandoutcomes_4.svg" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->
         <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_fo4_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__fo4_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">US Department of Education Integrated Postsecondary Education System (IPEDS) Human Resources Survey. Based on Fall 2013 data.</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/facultyqualityandoutcomes_4.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/faculty-quality-outcomes/#f4." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->

	<!------------------------fo5 chart half width----------------------- -->

		<div class="chartWrapperHalf">
				<a id="f5" class="shifted_anchor"></a>

		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/facultyqualityandoutcomes_5.svg" /><div class="chartFooter"><ul>
		 <!--jquery pop up- li item -->
             <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_fo5_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__fo5_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">US Department of Education Integrated Postsecondary Education System (IPEDS) Human Resources Survey. Based on Fall 2013 data.</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/facultyqualityandoutcomes_5.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/faculty-quality-outcomes/#f5." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->

		<!------------------------ fo6 chart chart full width----------------------- -->

		<div class="chartWrapperFull">
		<a id="f6" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/facultyqualityandoutcomes_6.png" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->
         <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_fo6_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__fo6_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">US Department of Education Integrated Postsecondary Education System (IPEDS) Human Resources Survey, <a href="http://nces.ed.gov/ipeds">http://nces.ed.gov/ipeds</a>. Based on provisional fall 2014 data.
			 <br />
		 	 <strong>Prepared by:</strong> Office of Institutional Research and Assessment (OIRA), August 23, 2016.</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/facultyqualityandoutcomes_6.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/faculty-quality-outcomes/#f6." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
		<!------------------------ fo7 chart chart full width----------------------- -->

		<div class="chartWrapperFull">
		<a id="f7" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/facultyqualityandoutcomes_7.png" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->
         <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_fo7_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__fo7_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">US Department of Education Integrated Postsecondary Education System (IPEDS) Human Resources Survey, <a href="http://nces.ed.gov/ipeds">http://nces.ed.gov/ipeds</a>. Based on provisional fall 2014 data.
			 <br />
		 	 <strong>Prepared by:</strong> Office of Institutional Research and Assessment (OIRA), August 23, 2016.</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/facultyqualityandoutcomes_7.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/faculty-quality-outcomes/#f7." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->

		<!------------------------fo8 chart full width----------------------- -->

		<div class="chartWrapperFull">
		<a id="f8" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/facultyqualityandoutcomes_8.png" /><div class="chartFooter"><ul>
		 <!--jquery pop up- li item -->
             <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_fo8_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__fo8_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">Collaborative on Academic Careers in Higher Education (COACHE) Survey, administered to UNC-Chapel Hill tenured and tenure track faculty during 2012-13.</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/facultyqualityandoutcomes_8.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/faculty-quality-outcomes/#f8." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
	<!------------------------fo9 chart Full width----------------------- -->

		<div class="chartWrapperFull">
		<a id="f9" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/facultyqualityandoutcomes_9.png" /><div class="chartFooter"><ul>
		 <!--jquery pop up- li item -->
             <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_fo9_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__fo9_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">Collaborative on Academic Careers in Higher Education (COACHE) Survey, administered to UNC-Chapel Hill tenured and tenure track faculty during 2012-13.</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/facultyqualityandoutcomes_9.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/faculty-quality-outcomes/#f9." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
<!------------------------ fo10 chart chart half width----------------------- -->

		<div class="chartWrapperHalf">
		<a id="f10" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/facultyqualityandoutcomes_10.png" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->
         <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_fo10_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__fo10_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">Compiled by the UNC-Chapel Hill Office of Institutional Research and Assessment using the 2014-15 Common Data Set from individual institutions' websites and the <em>U.S. News &amp; World Report</em>, America's Best Colleges, 2016 edition.  Based on Fall 2014 data.</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/facultyqualityandoutcomes_10.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/faculty-quality-outcomes/#f10." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
<!------------------------fo11 chart half width----------------------- -->

		<div class="chartWrapperHalf">
		<a id="f11" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/facultyqualityandoutcomes_11.png" /><div class="chartFooter"><ul>
		 <!--jquery pop up- li item -->
             <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_fo11_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__fo11_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">Compiled by the UNC-Chapel Hill Office of Institutional Research and Assessment using the 2014-15 Common Data Set from individual institutions' websites and the <em>U.S. News &amp; World Report</em>, America's Best Colleges, 2016 edition. Based on Fall 2014 data.</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/facultyqualityandoutcomes_11.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/faculty-quality-outcomes/#f11." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
	<!------------------------fo12 chart full width----------------------- -->

		<div class="chartWrapperFull">
		<a id="f12" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/facultyqualityandoutcomes_12.svg" /><div class="chartFooter"><ul>
		 <!--jquery pop up- li item -->
             <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_fo12_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__fo12_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">The Student Experience in the Research University Survey (SERU), administered to UNC-Chapel Hill undergraduates in Spring 2013.</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/facultyqualityandoutcomes_12.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/faculty-quality-outcomes/#f12." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->

<!------------------------ fo13 chart chart full width----------------------- -->

		<div class="chartWrapperFull">
		<a id="f13" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/facultyqualityandoutcomes_13.png" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->
         <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_fo13_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__fo13_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">The Student Experience in the Research University Survey (SERU), administered to UNC-Chapel Hill undergraduates in Spring 2013. </div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/facultyqualityandoutcomes_13.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/faculty-quality-outcomes/#f13." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
	<!------------------------ fo14 chart chart full width----------------------- -->

		<div class="chartWrapperFull">
		<a id="f14" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/facultyqualityandoutcomes_14.png" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->
         <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_fo14_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__fo14_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">National Science Foundation (NSF) Higher Education Research &amp; Development (HERD), <a href="http://www.nsf.gov/statistics/srvyherd">www.nsf.gov/statistics/srvyherd</a>. Based on FY 2014 data.
			 <br />
			 US Department of Education Integrated Postsecondary Education System (IPEDS) Human Resources Survey, <a href="http://nces.ed.gov/ipeds">http://nces.ed.gov/ipeds</a>. Based on provisional fall 2014 data.
			 <br />
		 	 <strong>Prepared by:</strong> Office of Institutional Research and Assessment (OIRA), August 21, 2016.</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/facultyqualityandoutcomes_14.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/faculty-quality-outcomes/#f14." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->

			<!------------------------fo15 chart Full width----------------------- -->

		<div class="chartWrapperFull">
		<a id="f15" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/facultyqualityandoutcomes_15.png" /><div class="chartFooter"><ul>
		 <!--jquery pop up- li item -->
             <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_fo15_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__fo15_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">
				Leiden Ranking 2016, <a href="http://www.leidenranking.com/">leidenranking.com</a>. Leiden University's Centre for Science and Technology Studies. Based on data from 2011–2014.
				<br />
				US Department of Education Integrated Postsecondary Education System (IPEDS) Human Resources Survey, <a href="http://www.nces.ed.gov/ipeds">nces.ed.gov/ipeds</a>. Based on provisional fall 2014 data.
 			 <br />
 		 	 <strong>Prepared by:</strong> Office of Institutional Research and Assessment (OIRA), August 22, 2016.</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/facultyqualityandoutcomes_15.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/faculty-quality-outcomes/#f15." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
		<!------------------------fo16 chart full width----------------------- -->

		<div class="chartWrapperFull">
		<a id="f16" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/facultyqualityandoutcomes_16.png" /><div class="chartFooter"><ul>
		 <!--jquery pop up- li item -->
             <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_fo16_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__fo16_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">Leiden Ranking 2015. Leiden University’s Centre for Science and Technology Studies. Based on data from 2010-13. <a href="http://www.leidenranking.com/">leidenranking.com</a></div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/facultyqualityandoutcomes_16.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/faculty-quality-outcomes/#f16." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
		<!------------------------ fo17 chart chart half width----------------------- -->

		<div class="chartWrapperHalf">
		<a id="f17" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/facultyqualityandoutcomes_17.png" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->
         <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_fo17_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__fo17_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">The Top American Research Universities 2013 Annual Report. Based on data collected through 2012. <a href="http://www.mup.asu.edu/">mup.asu.edu</a></div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/facultyqualityandoutcomes_17.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/faculty-quality-outcomes/#f17." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
		<!------------------------ fo18 chart chart half width----------------------- -->

		<div class="chartWrapperHalf">
		<a id="f18" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/facultyqualityandoutcomes_18.png" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->
         <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_fo18_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__fo18_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">The Top American Research Universities 2013 Annual Report. Based on data collected through 2012. <a href="http://www.mup.asu.edu/">mup.asu.edu</a></div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/facultyqualityandoutcomes_18.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/faculty-quality-outcomes/#f18." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->




			</div>	<!--close of content wrap-->




<?php get_footer(); ?>
