<?php
/*
Template Name: Gallery Page Template
*/
?>
<?php $thisPage="campus"; ?>
<?php get_header(); ?>
<div class="meantitle"><a href="http://carolinametrics.unc.edu" rel="nofollow"><?php bloginfo('name'); ?></a></div>


<a id="c" class="shifted_anchor"></a>

	<?php include("nav.php");?>
			<div class="banner bannerCampus"></div>

	<div id="contentwrap" class="clearfix">
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <h1 class="headline" id="logo"><?php the_title();?> </h1>
	<div class="statement"><?php the_content(); ?></div>
	<?php endwhile; else: ?>
	<p>Sorry, this page does not exist</p>

<?php endif; ?>

    <!----------------- ce1 chart half width----------------------- -->	
	
		<div class="chartWrapperHalf">
		 <a id="c1" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/CampusEnvironment-01.svg" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->	
         <li> 
		 <div class="p_anch">
			 <a href="#" class="js__p_ce1_start">Source</a>
		 </div>
		 
		 <div class="p_body js__p_body js__fadeout"></div>
		 
		  <div class="popup js__ce1_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">Collaborative on Academic Careers in Higher Education (COACHE) Survey, administered to UNC-Chapel Hill tenured and tenure track faculty during 2012-13.</div>
			 </div>
		</li>		
		<!--jquery pop up ends--> 
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/CampusEnvironment_1.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/campus-environment/#c1." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
		
	<!------------------------ce2 and ce3chart half width----------------------- -->	

		<div class="chartWrapperHalf">
		 <a id="c2" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/CampusEnvironment-02-03.svg" /><div class="chartFooter"><ul>
		 <!--jquery pop up- li item -->	
             <li> 
		 <div class="p_anch">
			 <a href="#" class="js__p_ce2_start">Source</a>
		 </div>
		 
		 <div class="p_body js__p_body js__fadeout"></div>
		 
		  <div class="popup js__ce2_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">The Student Experience in the Research University Survey (SERU), administered to UNC-Chapel Hill undergraduates in Spring 2013.</div>
			 </div>
		</li>	
		<!--jquery pop up ends--> 
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/CampusEnvironment-02-03.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/campus-environment/#c2." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
		
			<!------------------------ ce4 chart half width----------------------- -->	
	
		<div class="chartWrapperHalf">
		 <a id="c4" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/CampusEnvironment-04.svg" /><div class="chartFooter"><ul>
  	  <!--jquery pop up- li item -->	
         <li> 
		 <div class="p_anch">
			 <a href="#" class="js__p_ce4_start">Source</a>
		 </div>
		 
		 <div class="p_body js__p_body js__fadeout"></div>
		 
		  <div class="popup js__ce4_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content"><em>U.S. News &amp; World Report</em>, America's Best Colleges 2014. <a href="http://www.thebestcolleges.org/most-beautiful-campuses/"> thebestcolleges.org/most-beautiful-campuses/</a>, published in 2014.</div>
			 </div>
		</li>		
		<!--jquery pop up ends--> 
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/CampusEnvironment_4.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/campus-environment/#c4." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
		
	<!------------------------ce5 chart half width deleted america's most beautiful compus----------------------- -->	
		<!------------------------ce6 chart half width deleted universitas green tables----------------------- -->	
		<!------------------------ce7 chart half width----------------------- -->	
	<div class="chartWrapperHalf">
	 <a id="c7" class="shifted_anchor"></a>
    <img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/CampusEnvironment-07.svg" />
   
    <div class="chartFooter"><ul>
        <li> 
		 <div class="p_anch">
			 <a href="#" class="js__p_ce7_start">Source</a>
		 </div>
		 
		 <div class="p_body js__p_body js__fadeout"></div>
		 
		  <div class="popup js__ce7_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">Association for the Advancement of Sustainability in Higher Education's (AASHE) STARS Rating, published December 2014.</div>
			 </div>
		</li>	
       
        <li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/CampusEnvironment_7.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/campus-environment/#c7." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->		
		
			
			
			
				
	<!------------------------ce9 chart half width 8 and 9 switched----------------------- -->	

		<div class="chartWrapperHalf">
		 <a id="c9" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/CampusEnvironment-09.svg" /><div class="chartFooter"><ul>
  	  <!--jquery pop up- li item -->	
             <li> 
		 <div class="p_anch">
			 <a href="#" class="js__p_ce9_start">Source</a>
		 </div>
		 
		 <div class="p_body js__p_body js__fadeout"></div>
		 
		  <div class="popup js__ce9_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">Compiled by the UNC-Chapel Hill Office of Institutional Research and Assessment using the 2013-14 Common Data Set from individual institutions' websites and the <em>US News &amp; World Report</em>, America's Best Colleges, 2015 edition. Based on Fall 2013 data.</div>
			 </div>
		</li>	
		<!--jquery pop up ends--> 
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/CampusEnvironment_9.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/campus-environment/#c9." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
		
		<!------------------------ ce8 chart half width   8 and 9 switched----------------------- -->	
		<div class="chartWrapperHalf">
	 <a id="c8" class="shifted_anchor"></a>
    <img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/CampusEnvironment-08.svg" />
   
    <div class="chartFooter"><ul>
        <li> 
		 <div class="p_anch">
			 <a href="#" class="js__p_ce8_start">Source</a>
		 </div>
		 
		 <div class="p_body js__p_body js__fadeout"></div>
		 
		  <div class="popup js__ce8_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">Compiled by the UNC-Chapel Hill Office of Institutional Research and Assessment using the 2013-14 Common Data Set from individual institutions' websites and the <em>US News &amp; World Report</em>, America's Best Colleges, 2015 edition. Based on Fall 2013 data.</div>
			 </div>
		</li>	
       
        <li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/CampusEnvironment_8.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/campus-environment/#c8." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->		

		
			<!------------------------ce10 chart full width----------------------- -->	
					

			<!------------------------ce11 chart full width----------------------- -->	
			
		<!------------------------ ce12 chart half width----------------------- -->	
	
		<div class="chartWrapperHalf">
		 <a id="c12" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/CampusEnvironment-12.svg" /><div class="chartFooter"><ul>
  	  <!--jquery pop up- li item -->	
         <li> 
		 <div class="p_anch">
			 <a href="#" class="js__p_ce12_start">Source</a>
		 </div>
		 
		 <div class="p_body js__p_body js__fadeout"></div>
		 
		  <div class="popup js__ce12_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">Compiled by the UNC-CH Office of Institutional Research and Assessment from ConnectCarolina student information system. Based on Fall 2014 data.</div>
			 </div>
		</li>		
		<!--jquery pop up ends--> 
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/CampusEnvironment_12.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/campus-environment/#c12." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
		
	<!------------------------ce13 chart half width----------------------- -->	

		<div class="chartWrapperHalf">
		 <a id="c13" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/CampusEnvironment-13.svg" /><div class="chartFooter"><ul>
  	  <!--jquery pop up- li item -->	
             <li> 
		 <div class="p_anch">
			 <a href="#" class="js__p_ce13_start">Source</a>
		 </div>
		 
		 <div class="p_body js__p_body js__fadeout"></div>
		 
		  <div class="popup js__ce13_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">Compiled by the UNC-CH Office of Institutional Research and Assessment from ConnectCarolina student information system. Based on Fall 2014 data.</div>
			 </div>
		</li>	
		<!--jquery pop up ends--> 
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/CampusEnvironment_13.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/campus-environment/#c13." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
		
	<!------------------------ ce14 chart half width----------------------- -->	
	
		<div class="chartWrapperHalf">
		 <a id="c14" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/CampusEnvironment-14.svg"/><div class="chartFooter"><ul>
  	  <!--jquery pop up- li item -->	
         <li> 
		 <div class="p_anch">
			 <a href="#" class="js__p_ce14_start">Source</a>
		 </div>
		 
		 <div class="p_body js__p_body js__fadeout"></div>
		 
		  <div class="popup js__ce14_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">Collaborative on Academic Careers in Higher Education (COACHE) Survey, administered to UNC-Chapel Hill tenured and tenure track faculty during 2012-13.</div>
			 </div>
		</li>		
		<!--jquery pop up ends--> 
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/CampusEnvironment_14.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/campus-environment/#c14." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
		
	<!------------------------ce15 chart half width----------------------- -->	

		<div class="chartWrapperHalf">
		 <a id="c15" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/CampusEnvironment-15.svg"/><div class="chartFooter"><ul>
  	  <!--jquery pop up- li item -->	
             <li> 
		 <div class="p_anch">
			 <a href="#" class="js__p_ce15_start">Source</a>
		 </div>
		 
		 <div class="p_body js__p_body js__fadeout"></div>
		 
		  <div class="popup js__ce15_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">The Student Experience in the Research University Survey (SERU), administered to UNC-Chapel Hill undergraduates in Spring 2013.</div>
			 </div>
		</li>	
		<!--jquery pop up ends--> 
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/CampusEnvironment_15.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/campus-environment/#c115." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
	
	<!------------------------ce16 chart full width----------------------- -->	
	<div class="chartWrapperFull">
	 <a id="c16" class="shifted_anchor"></a>
    <img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/CampusEnvironment-16.svg" />
   
    <div class="chartFooter"><ul>
        <li> 
		 <div class="p_anch">
			 <a href="#" class="js__p_ce16_start">Source</a>
		 </div>
		 
		 <div class="p_body js__p_body js__fadeout"></div>
		 
		  <div class="popup js__ce16_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">Collaborative on Academic Careers in Higher Education (COACHE) Survey, administered to UNC-Chapel Hill tenured and tenure track faculty during 2012-13.</div>
			 </div>
		</li>	
       
        <li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/CampusEnvironment_16.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/campus-environment/#c16." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->		
		
<!------------------------ce17 chart full width----------------------- -->	
	<div class="chartWrapperFull">
	 <a id="c17" class="shifted_anchor"></a>
    <img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/CampusEnvironment-17.svg" />
   
    <div class="chartFooter"><ul>
        <li> 
		 <div class="p_anch">
			 <a href="#" class="js__p_ce17_start">Source</a>
		 </div>
		 
		 <div class="p_body js__p_body js__fadeout"></div>
		 
		  <div class="popup js__ce17_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">UNC-Chapel Hill Office of Research Information Systems, Vice Chancellor for Research.</div>
			 </div>
		</li>	
       
        <li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/CampusEnvironment_17.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/campus-environment/#c17." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->	
<!------------------------ce18 chart full width----------------------- -->	
	<div class="chartWrapperFull">
	 <a id="c18" class="shifted_anchor"></a>
    <img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/CampusEnvironment-18.svg" />
   
    <div class="chartFooter"><ul>
        <li> 
		 <div class="p_anch">
			 <a href="#" class="js__p_ce18_start">Source</a>
		 </div>
		 
		 <div class="p_body js__p_body js__fadeout"></div>
		 
		  <div class="popup js__ce18_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">Collaborative on Academic Careers in Higher Education (COACHE) Survey, administered to UNC-Chapel Hill tenured and tenure track faculty during 2012-13.</div>
			 </div>
		</li>	
       
        <li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/CampusEnvironment_18.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/campus-environment/#c18." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->	
		
		<!------------------------ ce19 chart half width----------------------- -->	
	
		<div class="chartWrapperHalf">
		 <a id="c19" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/CampusEnvironment-19.svg"/><div class="chartFooter"><ul>
  	  <!--jquery pop up- li item -->	
         <li> 
		 <div class="p_anch">
			 <a href="#" class="js__p_ce19_start">Source</a>
		 </div>
		 
		 <div class="p_body js__p_body js__fadeout"></div>
		 
		  <div class="popup js__ce19_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">UNC-Chapel Hill Employee Survey conducted by Carolina Counts in 2013.</div>
			 </div>
		</li>		
		<!--jquery pop up ends--> 
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/CampusEnvironment_19.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/campus-environment/#c19." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
		
	<!------------------------ce20 chart half width----------------------- -->	

		<div class="chartWrapperHalf">
		 <a id="c20" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/CampusEnvironment-20.svg"/><div class="chartFooter"><ul>
  	  <!--jquery pop up- li item -->	
             <li> 
		 <div class="p_anch">
			 <a href="#" class="js__p_ce20_start">Source</a>
		 </div>
		 
		 <div class="p_body js__p_body js__fadeout"></div>
		 
		  <div class="popup js__ce20_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">The Student Experience in the Research University Survey (SERU), administered to UNC-Chapel Hill undergraduates in Spring 2013.</div>
			 </div>
		</li>	
		<!--jquery pop up ends--> 
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/CampusEnvironment_20.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/campus-environment/#c20." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->

				
		
<!------------------------end of charts content----------------------- -->					
</div>	<!--close of content wrap-->
<?php get_footer(); ?>


