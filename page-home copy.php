<?php
/*
Template Name: Home
*/
?>

<?php
if( is_front_page() || is_home() ) get_header( 'splash');
else get_header( 'site' );?>


			<div id="content">

				<div id="inner-content" class="clearfix">

						<div id="main" class="twelvecol first clearfix" role="main">
<div id="circle-wrapper" class="">
<div class="circle1 transparent_class"><div class="circletextwrapper"><h1 class="circletext"><a href="http://localhost:8888/faculty-quality-outcomes/">Faculty Quality<br />&amp; Outcomes</a></h1></div></div>
   <div class="circle2 transparent_class"><div class="circletextwrapper"><h1 class="circletext"><a href="#">Public<br />Benefits</a></h1></div></div>
   <div class="circle3 transparent_class"><div class="circletextwrapper"><h1 class="circletext"><a href="#">Campus <br />Environment</a></h1></div></div>
      <div class="circle4 transparent_class "><div class="circletextwrapper"><h1 class="circletext"><a href="#">Student Quality
<br />&amp; Outcomes</a></h1></div> </div> 
<div id="tagline"><?php bloginfo('description'); ?></div>	<!--close of tag-->	
						<?php // to use a image just replace the bloginfo('name') with your img src and remove the surrounding <p> ?>
					<p id="logo" class="h1"><a href="<?php echo home_url("faculty-quality-outcomes"); ?>" rel="nofollow"><?php bloginfo('name'); ?></a></p>				


</div><!--close of circle-wrapper-->
							
									
						</div><!--close of main-->

						

				</div><!--close of inner content-->

			</div><!--close of content-->

<?php get_footer('footer2.php'); ?>
