<?php
/*
Template Name: Public Benefits
*/
?>
<?php $thisPage="public"; ?>
<?php get_header(); ?>
<div class="meantitle"><a href="http://carolinametrics.unc.edu" rel="nofollow"><?php bloginfo('name'); ?></a></div>


<a id="p" class="shifted_anchor"></a>
	<?php include("nav.php");?>
			<div class="banner bannerPublic"></div>

	<div id="contentwrap" class="clearfix">
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <h1 class="headline" id="logo"><?php the_title();?> </h1>
	<div class="statement"><?php the_content(); ?></div>
	<?php endwhile; else: ?>
	<p>Sorry, this page does not exist</p>

<?php endif; ?>
	<!------------------------pb1 chart full width----------------------- -->
	<div class="chartWrapperFull">
      <a id="p1" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/publicbenefits_1.png" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->
         <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_pb1_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__pb1_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">National Science Foundation (NSF) Higher Education Research &amp; Development Survey (HERD). Based on FY 2014 data. </div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/publicbenefits_1a.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/public-benefits/#p1." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
		<!------------------------pb2 chart full width----------------------- -->
	<div class="chartWrapperFull">
      <a id="p2" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/publicbenefits_2.png" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->
         <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_pb2_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__pb2_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">From 1981-1994: UNC-Chapel Hill Office of Sponsored Research Annual Reports.  From 1995 to present:  Office of the Vice Chancellor for Research website at <br /> <a href="http://research.unc.edu/about/facts-rankings/research-funding/index.htm">research.unc.edu/about/facts-rankings/research-funding/index.htm</a></div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/publicbenefits_2.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/public-benefits/#p2." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
		<!------------------------pb3 chart full width----------------------- -->
	<div class="chartWrapperFull">
      <a id="p3" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/publicbenefits_3.svg" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->
         <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_pb3_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__pb3_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">UNC-Chapel Hill Office of Research Information Systems, Vice Chancellor for Research.</a></div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/publicbenefits_3.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/public-benefits/#p3." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->

		<!------------------------pb4 chart full width----------------------- -->
	<div class="chartWrapperFull">
      <a id="p4" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/publicbenefits_4.png" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->
         <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_pb4_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__pb4_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">Association Of University Technology Managers (AUTM) FY 2014 data provided by Office of the Vice Chancellor for Research.</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/publicbenefits_4.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/public-benefits/#p4." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
		<!------------------------pb5 chart half width----------------------- -->
	<div class="chartWrapperHalf">
      <a id="p5" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/publicbenefits_5.svg" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->
         <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_pb5_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__pb5_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">US Department of Education Integrated Postsecondary Education System (IPEDS) Finance database. Based on FY 2012-13 data.</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/publicbenefits_5.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/public-benefits/#p5." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
			<!------------------------pb6 chart half width----------------------- -->
	<div class="chartWrapperHalf">
      <a id="p6" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/publicbenefits_6.png" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->
         <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_pb6_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__pb6_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content"><em>Kiplingers’s</em> Best College Values Report for 2015. Academic data based primarily on on Fall 2012; tuition data based on the 2013-14 academic year rates.</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/publicbenefits_6.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/public-benefits/#p6." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->

<!------------------------pb7 chart full width----------------------- -->
	<div class="chartWrapperFull">
      <a id="p7" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/publicbenefits_7.svg" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->
         <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_pb7_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__pb7_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">The Student Experience in the Research University Survey (SERU), administered to UNC-Chapel Hill undergraduates in Spring 2013.</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/publicbenefits_7.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/public-benefits/#p7." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
<!------------------------pb8 chart full width----------------------- -->
	<div class="chartWrapperFull">
      <a id="p8" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/publicbenefits_8.png" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->
         <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_pb8_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__pb8_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">Compiled by the UNC-Chapel Hill Office of Institutional Research &amp; Assessment from individual institutions' websites. Data reflect 2015-16 academic year rates.</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/publicbenefits_8.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/public-benefits/#p8." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
<!------------------------pb9 chart full width----------------------- -->
	<div class="chartWrapperFull">
      <a id="p9" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/publicbenefits_9.png" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->
         <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_pb9_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__pb9_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">Compiled by the UNC-Chapel Hill Office of Institutional Research and Assessment using the 2014-15 Common Data Set from individual institutions' websites and the <em>U.S. News &amp; World Report</em>, America's Best Colleges, 2016 edition.  Debt averages were based on the graduating classes of 2013-14.</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/publicbenefits_9.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/public-benefits/#p9." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
<!------------------------pb10 chart full width----------------------- -->
	<div class="chartWrapperFull">
      <a id="p10" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/publicbenefits_10.png" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->
         <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_pb10_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__pb10_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">UNC-Chapel Hill’s Carolina Center for Public Service. Data released July 2014. Note: Buckley Public Service Scholars complete 300 hours of public service, service learning courses, skills trainings, and a senior reflection activity in order to receive this special designation at graduation. </div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/publicbenefits_10.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/public-benefits/#p10." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
		<!------------------------pb11 chart full width----------------------- -->
	<div class="chartWrapperFull">
      <a id="p11" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/publicbenefits_11.png" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->
         <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_pb11_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__pb11_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">UNC-Chapel Hill Carolina Center for Public Service. Data released July 2014.</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/publicbenefits_11.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/public-benefits/#p11." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
<!------------------------pb12a chart full width----------------------- -->
	<div class="chartWrapperFull">
      <a id="p12a" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/publicbenefits_12a.png" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->
         <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_pb12a_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__pb12a_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">Peace Corp, <a href="http://www.peacecorps.gov">peacecorps.gov</a>, based on fiscal year data as of September 30, 2015
			 <br />
		 	 <strong>Prepared by:</strong> Office of Institutional Research and Assessment (OIRA), August 24, 2016.</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/publicbenefits_12a.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/public-benefits/#p12a." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
<!------------------------pb12b chart full width----------------------- -->
	<div class="chartWrapperFull">
      <a id="p12b" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/publicbenefits_12b.png" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->
         <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_pb12b_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__pb12b_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">Teach for America, <a href="https://www.teachforamerica.org">teachforamerica.org</a> as of August 2015.
			 <br />
		 	 <strong>Prepared by:</strong> Office of Institutional Research and Assessment (OIRA), August 24, 2016.</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/publicbenefits_12b.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/public-benefits/#p12b." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
<!------------------------pb13 chart full width----------------------- -->
	<div class="chartWrapperFull">
      <a id="p13" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/publicbenefits_13.png" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->
         <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_pb13_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__pb13_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">UNC General Alumni Association, as of July 2016.</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/publicbenefits_13.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/public-benefits/#p13." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
		<!------------------------pb14 chart half width----------------------- -->
	<div class="chartWrapperHalf">
      <a id="p14" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/publicbenefits_14.png" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->
         <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_pb14_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__pb14_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content"><em>U.S. News &amp; World Report</em>, America's Best Colleges, 2016 version. Based on Fall 2014 data.</div>
 			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/publicbenefits_14.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/public-benefits/#p14." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
		<!------------------------pb15 chart half width----------------------- -->
	<div class="chartWrapperHalf">
      <a id="p15" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/publicbenefits_15.png" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->
         <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_pb15_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__pb15_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content"> <em>Times Higher Education</em> World University Rankings for 2015-16, <a href="http://www.timeshighereducation.com">www.timeshighereducation.com</a>, based on fall 2014 data.
			 <br />
			 <strong>Prepared by:</strong> Office of Institutional Research and Assessment (OIRA), August 22, 2016</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/publicbenefits_15.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/public-benefits/#p15." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
		<!------------------------pb16 chart half width----------------------- -->
	<div class="chartWrapperHalf">
      <a id="p16" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/publicbenefits_16.png" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->
         <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_pb16_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__pb16_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">Shanghai Jiao Tong University Academic Ranking of World Universities for 2016, <a href="http://www.shanghairanking.com">www.shanghairanking.com</a>, based on Fall 2014 data.
			 <br />
			 <strong>Prepared by:</strong> Office of Institutional Research and Assessment (OIRA), August 22, 2016</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/publicbenefits_16.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/public-benefits/#p16." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div>
		<!--end of chartFooter-->
		</a>
		<!--close of anchortag-->
		<!------------------------pb17 chart half width----------------------- -->
	<div class="chartWrapperHalf">
      <a id="p17" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/publicbenefits_17.png" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->
         <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_pb17_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__pb17_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">Center for World University Rankings. Based on Fall 2014 data.</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/publicbenefits_17.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/public-benefits/#p17." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div>
		<!--end of chartFooter-->
		</a>
		<!--close of anchortag-->
		<!------------------------pb18 chart half width----------------------- -->
	<div class="chartWrapperHalf">
      <a id="p18" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/publicbenefits_18.svg" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->
         <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_pb18_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__pb18_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">US Department of Education Integrated Postsecondary Education System (IPEDS) Finance database. Based on FY 2012-13 data.</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/publicbenefits_18.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/public-benefits/#p18." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
		<!------------------------pb19 chart half width----------------------- -->
	<div class="chartWrapperHalf">
      <a id="p19" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/publicbenefits_19.svg" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->
         <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_pb19_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__pb19_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">Association of Research Libraries. Based on 2012-13 data.</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/publicbenefits_19.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/public-benefits/#p19." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
<!------------------------pb20 chart full width----------------------- -->
	<div class="chartWrapperFull">
      <a id="p20" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/publicbenefits_20.png" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->
         <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_pb20_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__pb20_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content"><em>U.S. News &amp; World Report</em>, America's Best Colleges, 2016 version. Based on 2014 data.</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/publicbenefits_20.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/public-benefits/#p20." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
		<!------------------------pb21 chart full width----------------------- -->
	<div class="chartWrapperFull">
      <a id="p21" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/publicbenefits_21.svg" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->
         <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_pb21_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__pb21_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">Compiled by the UNC-Chapel Hill Office of Institutional Research from university departments. Based on FY 2012-13 data.</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/publicbenefits_21.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/public-benefits/#p21." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->

			</div>	<!--close of content wrap-->




<?php get_footer(); ?>
