<?php
/*
Template Name: Student Quality & Outcomes
*/
?>
<?php $thisPage="student"; ?>
<?php get_header(); ?>
<div class="meantitle"><a href="http://carolinametrics.unc.edu" rel="nofollow"><?php bloginfo('name'); ?></a></div>


<a id="s" class="shifted_anchor"></a>
	<?php include("nav.php");?>
			<div class="banner bannerStudent"></div>

	<div id="contentwrap" class="clearfix">
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <h1 class="headline" id="logo"><?php the_title();?> </h1>
	<div class="statement"><?php the_content(); ?></div>
	<?php endwhile; else: ?>
	<p>Sorry, this page does not exist</p>

<?php endif; ?>


<!------------------------ sq1 chart half width----------------------- -->

		<div class="chartWrapperHalf">
		  <a id="s1" class="shifted_anchor"></a>
	<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/studentqualityandoutcomes_1.png" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->
         <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_sq1_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__sq1_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">Compiled by the UNC-Chapel Hill Office of Institutional Research and Assessment using the 2014-15 Common Data Set from individual institutions' websites and the <em>U.S. News & World Report</em>, America's Best Colleges, 2016 edition. Based on Fall 2014 data.</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/studentqualityandoutcomes_1.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/student-quality-outcomes/#s1." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
		<!------------------------ sq2 chart half width----------------------- -->

		<div class="chartWrapperHalf">
		  <a id="s2" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/studentqualityandoutcomes_2.png" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->
         <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_sq2_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__sq2_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content"><em>U.S. News &amp; World Report</em>, America's Best Colleges, 2016 edition. Based on Fall 2014 data.</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/studentqualityandoutcomes_2.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/student-quality-outcomes/#s2." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->


	<!------------------------sq3 chart full width----------------------- -->
	<div class="chartWrapperFull">
		  <a id="s3" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/studentqualityandoutcomes_3.png" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->
         <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_sq3_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__sq3_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content"><em>U.S. News &amp; World Report</em>, America's Best Colleges, 2016 edition. Based on Fall 2014 data.</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/studentqualityandoutcomes_3.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/student-quality-outcomes/#s3." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
		<!------------------------sq4 chart full width----------------------- -->
	<div class="chartWrapperFull">
		  <a id="s4" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/studentqualityandoutcomes_4.svg" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->
         <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_sq4_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__sq4_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">US Department of Education Integrated Postsecondary Education System (IPEDS) Student Financial Aid and Net Price database. Based on 2012-13 data.   Note: Students were identified as “low income” if they had received Pell Grants, which are federal financial aid funds offered to students who meet specific family resource requirements.</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/studentqualityandoutcomes_4.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/student-quality-outcomes/#s4." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->

	<!------------------------ sq5 chart fullwidth----------------------- -->

		<div class="chartWrapperFull">
		  <a id="s5" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/studentqualityandoutcomes_5.png" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->
         <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_sq5_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__sq5_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">US Department of Education Integrated Postsecondary Education System (IPEDS) Fall Enrollment. Based on Fall 2013 data. "Underrepresented Minority" includes the Hispanic/Latino, American Indian/Alaska Native, and Black/African American categories.</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/studentqualityandoutcomes_5.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/student-quality-outcomes/#s5." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
<!------------------------ sq6 chart full width----------------------- -->

		<div class="chartWrapperFull">
		  <a id="s6" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/studentqualityandoutcomes_6.png" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->
         <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_sq6_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__sq6_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">US Department of Education Integrated Postsecondary Education System (IPEDS) Fall Enrollment. Based on Fall 2013 data. "Underrepresented Minority" includes the Hispanic/Latino, American Indian/Alaska Native, and Black/African American categories.</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/studentqualityandoutcomes_6.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/student-quality-outcomes/#s6." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
		<!------------------------ sq7 full width----------------------- -->

		<div class="chartWrapperFull">
		  <a id="s7" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/studentqualityandoutcomes_7.png" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->
         <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_sq7_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__sq7_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">UNC-Chapel Hill Office of Scholarships and Student Aid. Based on 2014-15 data
				 <br />
				 	Updated by Office of Institutional Research and Assessment (OIRA) August 24, 2016
			 </div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/studentqualityandoutcomes_7.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/student-quality-outcomes/#s7." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->


<!------------------------sq8 chart full width----------------------- -->
	<div class="chartWrapperFull">
		  <a id="s8" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/studentqualityandoutcomes_8.svg" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->
         <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_sq8_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__sq8_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">The Student Experience in the Research University Survey (SERU), administered to UNC-Chapel Hill undergraduates in Spring 2013.</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/studentqualityandoutcomes_8.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/student-quality-outcomes/#s8." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
<!------------------------sq9 chart full width removed----------------------- -->
	<div class="chartWrapperFull">
		  <a id="s9" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/studentqualityandoutcomes_9.png" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->
         <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_sq9_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__sq9_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">US Department of Education Integrated Postsecondary Education System (IPEDS) Fall Enrollment.  Based on Fall 2013 data.</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/studentqualityandoutcomes_9.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/student-quality-outcomes/#s9." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->

		<!------------------------sq10 chart full width----------------------- -->
	<div class="chartWrapperFull">
		  <a id="s10" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/studentqualityandoutcomes_10.svg" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->
         <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_sq10_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__sq10_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">The UNC System Senior Survey, administered to UNC-Chapel Hill seniors in Spring 2013.</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/studentqualityandoutcomes_10.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/student-quality-outcomes/#s10." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
			<!------------------------ sq11 chart Full width----------------------- -->

		<div class="chartWrapperFull">
		  <a id="s11" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/studentqualityandoutcomes_11.svg" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->
         <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_sq11_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__sq11_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">The Student Experience in the Research University Survey (SERU), administered to UNC-Chapel Hill undergraduates in Spring 2013.</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/studentqualityandoutcomes_11.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/student-quality-outcomes/#s11." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
		<!------------------------ sq12 chart Full width----------------------- -->

		<div class="chartWrapperFull">
		  <a id="s12" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/studentqualityandoutcomes_12.png" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->
         <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_sq12_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__sq12_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">The AAU/Graduate School Exit Survey, administered to UNC-Chapel Hill graduating doctoral and master’s students in 2014-15.</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/studentqualityandoutcomes_12.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/student-quality-outcomes/#s12." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->

<!------------------------sq13 chart full width----------------------- -->
	<div class="chartWrapperFull">
		  <a id="s13" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/studentqualityandoutcomes_13.png" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->
         <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_sq13_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__sq13_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">UNC-Chapel Hill Office of Distinguished Scholarships. Data effective as of Summer 2016.</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/studentqualityandoutcomes_13.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/student-quality-outcomes/#s13." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
<!------------------------sq14 chart full width----------------------- -->
	<div class="chartWrapperFull">
		  <a id="s14" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/studentqualityandoutcomes_14.png" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->
         <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_sq14_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__sq14_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">US Department of Education Integrated Postsecondary Education System (IPEDS), Graduation Rates Report for 2013-14.</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/studentqualityandoutcomes_14.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/student-quality-outcomes/#s14." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
<!------------------------sq15 chart half width----------------------- -->
	<div class="chartWrapperHalf">
		  <a id="s15" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/studentqualityandoutcomes_15.png" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->
         <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_sq15_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__sq15_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">UNC-Chapel Hill Office of Institutional Research and Assessment. Graduation status of 2007 entering first-year cohort as of Fall 2013. Note: Students were identified as “low income” if they had received Pell Grants, which are federal financial aid funds offered to students who meet specific family resource requirements.</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/studentqualityandoutcomes_15.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/student-quality-outcomes/#s15." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
<!------------------------sq16 chart half width----------------------- -->
	<div class="chartWrapperHalf">
		  <a id="s16" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/studentqualityandoutcomes_16.png" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->
         <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_sq16_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__sq16_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">UNC-Chapel Hill Office of Institutional Research and Assessment. Graduation status of 2008 entering first-year cohort as of July 24, 2015.</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/studentqualityandoutcomes_16.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/student-quality-outcomes/#s16." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
<!------------------------ sq17 chart Full width----------------------- -->

		<div class="chartWrapperFull">
		  <a id="s17" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/studentqualityandoutcomes_17.png" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->
         <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_sq17_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__sq17_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">UNC-Chapel Hill Office of Institutional Research and Assessment. Graduation status of 2008 entering sophomore transfers and 2009 entering junior transfer students as of July 27, 2015.</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/studentqualityandoutcomes_17.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/student-quality-outcomes/#s17." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
		<!------------------------ sq18 chart full width----------------------- -->

		<div class="chartWrapperFull">
		  <a id="s18" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/studentqualityandoutcomes_18.png" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->
         <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_sq18_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__sq18_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">The Graduate School, UNC-Chapel Hill.</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/studentqualityandoutcomes_18.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/student-quality-outcomes/#s18." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
		<!------------------------ sq19 chart full width----------------------- -->

		<div class="chartWrapperFull">
		  <a id="s19" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/studentqualityandoutcomes_19.png" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->
         <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_sq19_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__sq19_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">Undergraduate First Destination Survey, UNC-Chapel Hill University Career Services. Administered in Fall 2014 to students who received bachelor’s degrees in May 2014.</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/studentqualityandoutcomes_19.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/student-quality-outcomes/#s19." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
		<!------------------------ sq20 chart full width----------------------- -->

		<div class="chartWrapperFull">
		  <a id="s20" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/studentqualityandoutcomes_20.png" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->
         <li>
		 <div class="p_anch">
			 <a href="#" class="js__p_sq20_start">Source</a>
		 </div>

		 <div class="p_body js__p_body js__fadeout"></div>

		  <div class="popup js__sq20_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">The AAU/Graduate School Exit Survey, administered to UNC-Chapel Hill graduating doctoral and master’s students in 2014-15.</div>
			 </div>
		</li>
		<!--jquery pop up ends-->
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/studentqualityandoutcomes_20.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/student-quality-outcomes/#s20." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->


			</div>	<!--close of content wrap-->




<?php get_footer(); ?>
