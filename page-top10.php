<?php
/*
Template Name: Top Ten
*/
?>
<?php $thisPage="top10"; ?>
<?php get_header(); ?>
<div class="meantitle"><a href="http://carolinametrics.unc.edu" rel="nofollow"><?php bloginfo('name'); ?></a></div>

 <a id="top" class="shifted_anchor"></a>

	<?php include("nav.php");?>
			<div class="banner bannertop10"></div>
	<div id="contentwrap" class="clearfix">
    <h1 class="headline" id="logo">Top 10</h1>
	<!------------------------top10-1 chart full width----------------------- -->	
	<div class="chartWrapperFull">
      <a id="top1" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/topten_1.png" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->	
         <li> 
		 <div class="p_anch">
			 <a href="#" class="js__p_top1_start">Source</a>
		 </div>
		 
		 <div class="p_body js__p_body js__fadeout"></div>
		 
		  <div class="popup js__top1_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			<div class="p_content">US Department of Education Integrated Postsecondary Education System (IPEDS), Graduation Rates Report for 2013-14</div>
			 </div>
		</li>		
		<!--jquery pop up ends--> 
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/topten_1.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/top-10/#top1." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
		<!------------------------top10-2 chart full width----------------------- -->	
	<div class="chartWrapperFull">
      <a id="top2" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/studentqualityandoutcomes_4.svg" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->	
         <li> 
		 <div class="p_anch">
			 <a href="#" class="js__p_top2_start">Source</a>
		 </div>
		 
		 <div class="p_body js__p_body js__fadeout"></div>
		 
		  <div class="popup js__top2_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
		<div class="p_content">US Department of Education Integrated Postsecondary Education System (IPEDS) Student Financial Aid and Net Price database. Based on 2012-13 data. Note: Students were identified as “low income” if they had received Pell Grants, which are federal financial aid funds offered to students who meet specific family resource requirements.</div>
			 </div>
		</li>		
		<!--jquery pop up ends--> 
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/studentqualityandoutcomes_4.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/top-10/#top2." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
		<!------------------------top10-3 chart full width----------------------- -->	
	<div class="chartWrapperFull">
      <a id="top2" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/studentqualityandoutcomes_5.svg" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->	
         <li> 
		 <div class="p_anch">
			 <a href="#" class="js__p_top3_start">Source</a>
		 </div>
		 
		 <div class="p_body js__p_body js__fadeout"></div>
		 
		  <div class="popup js__top3_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">US Department of Education Integrated Postsecondary Education System (IPEDS) Fall Enrollment. Based on Fall 2013 data. "Underrepresented Minority" includes the Hispanic/Latino, American Indian/Alaska Native, and Black/African American categories.</div>
			 </div>
		</li>		
		<!--jquery pop up ends--> 
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/studentqualityandoutcomes_5.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/top-10/#top3." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
		
		<!------------------------top10-4 chart half width----------------------- -->	
	<div class="chartWrapperHalf">
      <a id="top3" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/topten_4.svg" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->	
         <li> 
		 <div class="p_anch">
			 <a href="#" class="js__p_top4_start">Source</a>
		 </div>
		 
		 <div class="p_body js__p_body js__fadeout"></div>
		 
		  <div class="popup js__top4_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			<div class="p_content">US Department of Education Integrated Postsecondary Education System (IPEDS) Human Resources Survey. Based on Fall 2013 data.</div>
			 </div>
		</li>		
		<!--jquery pop up ends--> 
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/topten_4.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/top-10/#top4." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
		
		
		<!------------------------top10-5 chart half width----------------------- -->	
		
		
	<div class="chartWrapperHalf">
      <a id="top5" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/topten_5.svg" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->	
         <li> 
		 <div class="p_anch">
			 <a href="#" class="js__p_top5_start">Source</a>
		 </div>
		 
		 <div class="p_body js__p_body js__fadeout"></div>
		 
		  <div class="popup js__top5_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			<div class="p_content">US Department of Education Integrated Postsecondary Education System (IPEDS) Finance database. Based on FY 2012-13 data.</div>
			 </div>
		</li>		
		<!--jquery pop up ends--> 
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/topten_5.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/top-10/#top5." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
			<!------------------------top10-6 chart full width----------------------- -->	
	<div class="chartWrapperFull">
      <a id="top6" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/publicbenefits_2.svg" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->	
         <li> 
		 <div class="p_anch">
			 <a href="#" class="js__p_top6_start">Source</a>
		 </div>
		 
		 <div class="p_body js__p_body js__fadeout"></div>
		 
		  <div class="popup js__top6_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">From 1981-1994: UNC-Chapel Hill Office of Sponsored Research Annual Reports.  From 1995 to present:  Office of the Vice Chancellor for Research website at <br /> <a href="http://research.unc.edu/about/facts-rankings/research-funding/index.htm">research.unc.edu/about/facts-rankings/research-funding/index.htm</a></div>
			 </div>
		</li>		
		<!--jquery pop up ends--> 
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/publicbenefits_2.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/top-10/#top6." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->

<!------------------------top10-7 chart full width----------------------- -->	
	<div class="chartWrapperFull">
      <a id="top7" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/facultyqualityandoutcomes_16.png" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->	
         <li> 
		 <div class="p_anch">
			 <a href="#" class="js__p_top7_start">Source</a>
		 </div>
		 
		 <div class="p_body js__p_body js__fadeout"></div>
		 
		  <div class="popup js__top7_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			 <div class="p_content">Leiden Ranking 2015. Leiden University’s Centre for Science and Technology Studies. Based on data from 2010-13. <a href="http://www.leidenranking.com">leidenranking.com</a></div>
			 </div>
		</li>		
		<!--jquery pop up ends--> 
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/facultyqualityandoutcomes_16.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/top-10/#top7." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
<!------------------------top10-8 chart half width----------------------- -->	
	<div class="chartWrapperHalf">
      <a id="top8" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/topten_08.png" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->	
         <li> 
		 <div class="p_anch">
			 <a href="#" class="js__p_top8_start">Source</a>
		 </div>
		 
		 <div class="p_body js__p_body js__fadeout"></div>
		 
		  <div class="popup js__top8_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			<div class="p_content"><em>U.S. News & World Report</em>, America's Best Colleges, 2016 version. Based on Fall 2014 data.</div>
			 </div>
		</li>		
		<!--jquery pop up ends--> 
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/topten_08.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/top-10/#top8." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
<!------------------------top10-9 chart half width----------------------- -->	
	<div class="chartWrapperHalf">
      <a id="top9" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/publicbenefits_16.png" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->	
         <li> 
		 <div class="p_anch">
			 <a href="#" class="js__p_top9_start">Source</a>
		 </div>
		 
		 <div class="p_body js__p_body js__fadeout"></div>
		 
		  <div class="popup js__top9_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
			<div class="p_content">Shanghai Jiao Tong University Academic Ranking of World Universities for 2015. Based on Fall 2013 data.</div>
			 </div>
		</li>		
		<!--jquery pop up ends--> 
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/publicbenefits_16.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/top-10/#top9." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
<!------------------------top10-10 chart full width----------------------- -->	
	<div class="chartWrapperFull">
      <a id="top10" class="shifted_anchor"></a>
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/publicbenefits_20.png" /><div class="chartFooter"><ul>
  	   <!--jquery pop up- li item -->	
         <li> 
		 <div class="p_anch">
			 <a href="#" class="js__p_top10_start">Source</a>
		 </div>
		 
		 <div class="p_body js__p_body js__fadeout"></div>
		 
		  <div class="popup js__top10_popup js__slide_top">
			  <a href="#" class="p_close js__p_close" title="Close">
				 <span></span><span></span>
			 </a>
	<div class="p_content"><em>U.S. News &amp; World Report</em>, America's Best Colleges, 2016 version. Based on 2014 data.</div>
			 </div>
		</li>		
		<!--jquery pop up ends--> 
		<li><a href="<?php echo get_template_directory_uri(); ?>/library/images/downloads/publicbenefits_20.pdf"><span aria-hidden="true" data-icon="&#xe601"></span></a></li></li>
		<li><a href="mailto:?subject=Carolina Metrics&amp;body=Follow the link to view metric - http://carolinametric.wpengine.com/top-10/#top10." title="Share by Email"><span aria-hidden="true" data-icon="&#xe600"></span></a></li></ul></div>
		</div><!--end of chartFooter-->
		</a><!--close of anchortag-->
		

		
		
		
			</div>	<!--close of content wrap-->			
			

				

<?php get_footer(); ?>





